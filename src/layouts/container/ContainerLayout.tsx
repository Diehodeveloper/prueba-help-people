import { FC } from 'react'
import { Button } from 'zzz-folder03-componentes-react-ts/buttons'
import './ContainerLayout.scss'

interface Container {
	children: any,
	title?: string,
	buttonTitle?: string,
	containsButton?: boolean,
	onHandleCreate?: React.MouseEventHandler<HTMLButtonElement>
}

export const ContainerLayout: FC<Container> = ({ children, title, buttonTitle, containsButton, onHandleCreate }) => {

	return (
		<div className='container-layout'>
			<h1>{title}</h1>

			{ containsButton &&
				<Button
					title={buttonTitle ? buttonTitle : ''}
					onClick={onHandleCreate}
					className={'button-create'}
					type='button'
				/>
			}

			{children}
		</div>
	)
}