
import { enumOpcionPost } from "../../shared/enums/enumOpcionPost"
import { startDeleteCitizen, startResetCitizenById } from "../../store/citizens/Citizens.Thunk"
import { ModalFullScreen, ModalConfirmation } from "zzz-folder03-componentes-react-ts/modals"

import './CitizenPage.scss'
import { setPostOptionAction, toogleModalFullScreenAction } from "../../shared/hooks/useView"
import { useBasesDispatch, useBasesSelector } from "../../shared/store/store"
import { CreateCitizen } from "./create/CreateCitizen"
import { UpdateCitizen } from "./update/UpdateCitizen"
import { TableCitizens } from "./table/TableCitizens"
import { ContainerLayout } from "../../layouts/container/ContainerLayout"

export const CitizenPage = () => {
	const { isModalFullScreenOpen, postOption, idForPost } = useBasesSelector( state => state.View )
	const dispatch = useBasesDispatch()
	
	const handleDelete = () => {
		dispatch( startDeleteCitizen( idForPost ) )
		dispatch( toogleModalFullScreenAction() )
	}
	
	const handleCancel = () => {
        dispatch( toogleModalFullScreenAction())
    }
	
	const onClickCreate = () => {
        dispatch( setPostOptionAction( enumOpcionPost.create ) )
		dispatch( startResetCitizenById() )
		dispatch( toogleModalFullScreenAction() )
	}
	const getForm = ( postOption: number ) => {
		switch (postOption) {
			case enumOpcionPost.create:
				return <ModalFullScreen children={ <CreateCitizen /> } />
			case enumOpcionPost.update: 
				return <ModalFullScreen children={ <UpdateCitizen /> }  />
			case enumOpcionPost.delete: 
				return <>
				    <ModalFullScreen children={ 
					    <ModalConfirmation 
							id={ idForPost } 
							onClickCancel={ handleCancel } 
							onClickConfirm={ handleDelete }
							title='ciudadano'
					    ></ModalConfirmation> 
					}></ModalFullScreen>
				</>
		}
	}
	
	return (
		<ContainerLayout
			title={ 'Lista de ciudadanos' } 
			buttonTitle={ 'Agregar ciudadano' } 
			onHandleCreate={ onClickCreate }
			containsButton={ true }
		>
		
			<div className="citizen-page">
				<TableCitizens />
				
				{ isModalFullScreenOpen && getForm( postOption ) }
			</div>	
		</ContainerLayout>  
	)
}

