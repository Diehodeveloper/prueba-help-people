
import React, { useEffect } from "react"
import { useForm } from "../../../shared/hooks/useForm"
import { enumTypesInput } from "../../../shared/enums/enumTypesInput"
import { listDocumentType } from "../../../shared/const/lists"
import { IFormlabels } from "zzz-folder03-componentes-react-ts/forms/IForm"
import { useBasesDispatch, useBasesSelector } from "../../../shared/store/store"
import { toogleModalFullScreenAction } from "../../../shared/hooks/useView"
import { Form } from "zzz-folder03-componentes-react-ts/forms"
import { startCreateCitizen } from "../../../store/citizens/Citizens.Thunk"
import './CreateCitizen.scss'
import { enumCitizenAttribute } from "../../../shared/enums/enumCitizenAttributes"


const labels: IFormlabels[] = [ 
	{ key: 'id', title: enumCitizenAttribute.id, typeInput: enumTypesInput.text },
	{ key: 'documentType', title: enumCitizenAttribute.documentType, typeInput: enumTypesInput.select, placeholder: 'Seleccione el tipo de documento', options: listDocumentType },
	{ key: 'documentNumber', title: enumCitizenAttribute.documentNumber, typeInput: enumTypesInput.text, isRequired: true, placeholder: 'Ej: 178767362' },
	{ key: 'names', title: enumCitizenAttribute.names, typeInput: enumTypesInput.text, isRequired: true, placeholder: 'Ej: Juan Pablo' },
	{ key: 'surnames', title: enumCitizenAttribute.surnames, typeInput: enumTypesInput.text, placeholder: 'Ej: Valdes Sanchez' },
	{ key: 'birthday', title: enumCitizenAttribute.birthday, typeInput: enumTypesInput.text, placeholder: 'Seleccione una fecha' },
	{ key: 'profession', title: enumCitizenAttribute.proffesion, typeInput: enumTypesInput.text, placeholder: 'Ej: Desarrollador' },
	{ key: 'salarialAspiration', title: enumCitizenAttribute.salaryAspiration, typeInput: enumTypesInput.text, placeholder: 'Ej: 3.000.000' },
	{ key: 'email', title: enumCitizenAttribute.email, typeInput: enumTypesInput.email, placeholder: 'Ej: miCorreo@dominio.com' },
]

const skyKeys = [ 'id' ]

export const CreateCitizen = () => {

	const dispatch = useBasesDispatch()
	const { CiticieById } = useBasesSelector( state => state.Citizens)
	const { onHandleBlur, 
			onInputChange, 
			formState, 
			onMultipleSelectChange,
			 onSelectChange, 
			 setFormState, 
			 onCheckboxChange } = useForm ( CiticieById )

	useEffect( () => {
		setFormState( CiticieById )
	}, [ CiticieById ])
 
	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault()
		dispatch( startCreateCitizen( formState ) )
		dispatch( toogleModalFullScreenAction() )
	}
	
	const handleCancel = () => {
        dispatch( toogleModalFullScreenAction())
    }
	
	return (
		<div className="create-citizen">
			<Form
				botonCloseTitle={ "Cancelar" }
				botonSaveTitle={ "Crear un ciudadano!" } 
				labels={ labels }
				object={ formState }
				onCheckboxChange={ onCheckboxChange}
				onClickCalcel={ handleCancel }			
				onInputChange={ onInputChange }
				onMultipleSelectChange={ onMultipleSelectChange }
				onSelectChange={ onSelectChange }
				onSubmit={ handleSubmit }
				skipkeys={ skyKeys }
				title={ "Creemos un nuevo ciudadano" }
				onBlur={ onHandleBlur }
			/>
		</div>
	)
}