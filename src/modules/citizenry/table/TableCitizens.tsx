
import React, { FC } from 'react'
import { useEffect } from 'react'
import { Table, TableColumn } from 'zzz-folder03-componentes-react-ts/tables/Table'
import { enumOpcionPost } from '../../../shared/enums/enumOpcionPost'
import { useBasesDispatch, useBasesSelector } from '../../../shared/store/store'
import { setIdForPostAction, setPostOptionAction, toogleModalFullScreenAction } from '../../../shared/hooks/useView'
import { startGetCitizens, startGetCitizenById } from '../../../store/citizens/Citizens.Thunk'
import { enumIcons } from '../../../shared/enums/enumIcons'
import './TableCitizens.scss'
import { enumCitizenAttribute } from '../../../shared/enums/enumCitizenAttributes'
import { listDocumentType } from '../../../shared/const/lists'


export const TableCitizens: FC = () => {

	const dispatch = useBasesDispatch()
	const { Citizens  } = useBasesSelector( state => state.Citizens )

	useEffect(() => {
		dispatch( startGetCitizens())
	}, [ ])
	
	const onHandleDelete = (event: React.MouseEvent<HTMLSpanElement>) => {
		dispatch( setPostOptionAction( enumOpcionPost.delete ) )
		const id = event.currentTarget.parentElement?.parentElement?.parentElement?.id
		
		if ( id ) {
			dispatch( setIdForPostAction( + id ) )
			dispatch( toogleModalFullScreenAction() )
		}
	}
	
	const onHandleUpdate = async(event: React.MouseEvent<HTMLSpanElement>) => {
		const id = event.currentTarget.parentElement?.parentElement?.parentElement?.id
		dispatch( setPostOptionAction( enumOpcionPost.update ))
		
		if ( id ) {
			await dispatch( startGetCitizenById( + id ))
			dispatch( toogleModalFullScreenAction() )
		}
	}
	
	const customContent = <div className='table-users__custom-opcions'>
		<i className={ enumIcons.update } onClick={ onHandleUpdate } />
		<i className={ enumIcons.delete } onClick={ onHandleDelete } />
	</div>
	
	const columns: TableColumn[]  = [
		{ key: 'documentType', titleHeader: enumCitizenAttribute.documentType, textAlignHeader: 'start', list: listDocumentType },
		{ key: 'documentNumber', titleHeader: enumCitizenAttribute.documentNumber, textAlignHeader: 'start' },
		{ key: 'names', titleHeader: enumCitizenAttribute.names, textAlignHeader: 'start' },
		{ key: 'surnames', titleHeader: enumCitizenAttribute.surnames, textAlignHeader: 'start' },
		{ key: 'birthday', titleHeader: enumCitizenAttribute.birthday, textAlignHeader: 'start' },
		{ key: 'profession', titleHeader: enumCitizenAttribute.proffesion, textAlignHeader: 'start' },
		{ key: 'salarialAspiration', titleHeader: enumCitizenAttribute.salaryAspiration, textAlignHeader: 'start' },
		{ key: 'email', titleHeader: enumCitizenAttribute.email, textAlignHeader: 'start' },
		{ key: '', titleHeader: 'Acciones', haveCustomContent: true,  },
	]

	return <section className='table-citizens'>
	 <Table
			data={ Citizens } 
			columns={ columns }
			haveFormat={ true }
			customContent={ customContent }
		/>   
	</section>
}

	