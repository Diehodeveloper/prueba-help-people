
import React, { FC, useEffect } from "react"
import { useForm } from "../../../shared/hooks/useForm"
import { Form } from "zzz-folder03-componentes-react-ts/forms"
import { enumAlertsColors } from "../../../shared/enums/enumColorAlerts"
import { enumAlerts } from "../../../shared/enums/enumAlerts"
import { listDocumentType } from "../../../shared/const/lists"
import { enumTypesInput } from "../../../shared/enums/enumTypesInput"
import { IFormlabels } from "zzz-folder03-componentes-react-ts/forms/IForm"
import { useBasesDispatch, useBasesSelector } from "../../../shared/store/store"
import { startSetColorAlert, startSetTextAlert, toogleModalFullScreenAction } from "../../../shared/hooks/useView"
import { startUpdateCitizen } from "../../../store/citizens/Citizens.Thunk"
import { enumCitizenAttribute } from "../../../shared/enums/enumCitizenAttributes"
import './UpdateCitizen.scss'

const skyKeys = [ 'id' ]

export const UpdateCitizen: FC = () => {

	const dispatch = useBasesDispatch()
	const { CiticieById } = useBasesSelector( state => state.Citizens )
	const { onHandleBlur,
			onInputChange, 
			formState, 
			setFormState, 
			onSelectChange, 
			onMultipleSelectChange,
			onCheckboxChange } = useForm ( CiticieById )
	
	useEffect( () => {
		setFormState(CiticieById)
	}, [ CiticieById ])

	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault()
		dispatch( startUpdateCitizen( formState, CiticieById.id ) )
		dispatch( toogleModalFullScreenAction() )
		startSetColorAlert( enumAlertsColors.success )
		startSetTextAlert( enumAlerts.successUpdate )
	}
	
	const handleCancel = () => {
        dispatch( toogleModalFullScreenAction())
    }
  
	const labels: IFormlabels[] = [ 
		{ key: 'id', title: enumCitizenAttribute.id },
		{ key: 'documentType', title: enumCitizenAttribute.documentType, typeInput: enumTypesInput.select, options: listDocumentType },
		{ key: 'documentNumber', title: enumCitizenAttribute.documentNumber },
		{ key: 'names', title: enumCitizenAttribute.names },
		{ key: 'surnames', title: enumCitizenAttribute.surnames, isRequired: false },
		{ key: 'birthday', title: enumCitizenAttribute.birthday, typeInput: enumTypesInput.text },
		{ key: 'profession', title: enumCitizenAttribute.proffesion, typeInput: enumTypesInput.text },
		{ key: 'salarialAspiration', title: enumCitizenAttribute.salaryAspiration, typeInput: enumTypesInput.text },
		{ key: 'email', title: enumCitizenAttribute.email, typeInput: enumTypesInput.text },	
	]
	
	return (
		<div className="update-user">
			<Form
				botonCloseTitle={ "Cancelar" }
				botonSaveTitle={ "Actualizar usuario" } 
				labels={ labels }
				object={ formState }
				onClickCalcel={ handleCancel }			
				onInputChange={ onInputChange }
				onMultipleSelectChange={ onMultipleSelectChange }
				onSelectChange={ onSelectChange }
				onSubmit={ handleSubmit }
				skipkeys={ skyKeys }
				title={ "Actualizar el usuario" }
				onCheckboxChange={ onCheckboxChange }
				onBlur={ onHandleBlur }
			/>
		</div>
	)
}