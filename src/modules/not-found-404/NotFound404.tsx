import { NavLink } from 'react-router-dom'

import { getCurrentYear } from '../../shared/functions/functions'
import { enumPath } from '../../shared/enums/enumPath'
import './NotFound404.scss'

export const NotFound404 =  () => {

	return (
		<>	
			<section className='NotFound404-page'>
				<div className='NotFound404__header'>
				    <span>404</span>
					<div>
						<h2>Sorry!</h2>
						<h3>Parece que a donde vas no es a donde podemos ir...</h3>
					</div>
				</div>
				<NavLink to={ enumPath.citizens }> Regresar al inicio</NavLink>
				<div className='NotFound404__copy-right'>
					<span>{ getCurrentYear( )} Created By Diego Rojas </span>
				</div>
			</section>  
		</>
	)
}
