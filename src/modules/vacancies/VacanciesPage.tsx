import { ModalFullScreen, ModalConfirmation } from 'zzz-folder03-componentes-react-ts/modals'
import { toogleModalFullScreenAction } from '../../shared/hooks/useView'
import { useBasesDispatch, useBasesSelector } from '../../shared/store/store'
import { enumOpcionPost } from '../../shared/enums/enumOpcionPost'
import { UpdateVacancy } from './update/UpdateVacancy'
import { TableProjects } from './table/TableVacancies'
import './VacanciesPage.scss'
import { ContainerLayout } from '../../layouts/container/ContainerLayout'

export const VacanciePage = () => {	
	const { isModalFullScreenOpen, postOption, idForPost } = useBasesSelector( state => state.View )
	const dispatch = useBasesDispatch()
	
	const handleCancel = () => {
        dispatch( toogleModalFullScreenAction())
    }
	
	const getForm = ( postOption: number ) => {
		switch ( postOption ) {
			case enumOpcionPost.update: 
				return <ModalFullScreen children={ <UpdateVacancy /> } />
		}
	}					
						
	return (
		<ContainerLayout 
			title={ 'Lista de vacantes' } 
			containsButton={ false }
		>
		
			<div className="vacancies-page">
				<TableProjects />
				{ isModalFullScreenOpen && getForm( postOption ) }
			</div>
			
		</ContainerLayout>  
	)
}
