import React, { FC } from 'react'
import { useEffect } from 'react'
import { enumOpcionPost } from '../../../shared/enums/enumOpcionPost'
import { Table } from 'zzz-folder03-componentes-react-ts/tables'

import './TableVacancies.scss'
import { enumColors } from '../../../shared/enums/enumColors'
import { enumIcons } from '../../../shared/enums/enumIcons'
import { enumVacancyStatus } from '../../../shared/enums/enumProjectStatus'
import { useBasesDispatch, useBasesSelector } from '../../../shared/store/store'
import { setIdForPostAction, setPostOptionAction, toogleModalFullScreenAction } from '../../../shared/hooks/useView'
import { TableColumn } from 'zzz-folder03-componentes-react-ts/tables/Table'
import { enumVacanciesAttributes } from '../../../shared/enums/enumVacanciesAttribute'
import { startGetVacancies, startGetVacancyId } from '../../../store/vacancies/Vacancies.Thunk'


export const TableProjects: FC = () => {
	
	const dispatch = useBasesDispatch()
	
	useEffect(() => {
	 dispatch( startGetVacancies() )
	}, [])
	
	const { Vacancies  } = useBasesSelector( state => state.Vacancies )
	
	const onHandleUpdate = async( event: React.MouseEvent<HTMLSpanElement> ) => {
		const id = event.currentTarget.parentElement?.parentElement?.parentElement?.id
		dispatch( setPostOptionAction( enumOpcionPost.update ) )
		
		if ( id ) {
			await dispatch( startGetVacancyId( + id ) )
			dispatch (toogleModalFullScreenAction() )
		}
	}
	
	const customContent = <div className='table-projects__custom-opcions'>
		<i className={ enumIcons.update } onClick={ onHandleUpdate } />
	</div>
	
	const columns: TableColumn[]  = [
		    { key: 'id', titleHeader: enumVacanciesAttributes.id, textAlignHeader: 'start' },
			{ key: 'code', titleHeader: enumVacanciesAttributes.code, textAlignHeader: 'start' },
			{ key: 'rol', titleHeader: enumVacanciesAttributes.rol, textAlignHeader: 'start' },
			{ key: 'description', titleHeader: enumVacanciesAttributes.description, textAlignHeader: 'start' },
			{ key: 'enterprise', titleHeader: enumVacanciesAttributes.enterprise, textAlignHeader: 'start' },
			{ key: 'salary', titleHeader: enumVacanciesAttributes.salary, textAlignHeader: 'start' },
			{ key: 'idCitizen', titleHeader: enumVacanciesAttributes.citizenId, textAlignHeader: 'start' },
			{ key: 'status', titleHeader: 'Estado', validationList: [ enumVacancyStatus.asigned ], backgroundSpan:'var(--yellow)'},
			{ key: '', titleHeader: 'Acciones', haveCustomContent: true, textAlignHeader: 'center' },
		] 
	
		return <section className='table-projects'>
			<Table 
				data={ Vacancies } 
				columns={ columns } 
				haveFormat={ false }
				customContent={ customContent }
			/>
		</section>
	}
	
	