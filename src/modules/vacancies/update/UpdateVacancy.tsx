import React, { FC, useEffect } from "react"
import { useForm } from "../../../shared/hooks/useForm"
import { Form } from "zzz-folder03-componentes-react-ts/forms"
import { ListEnterphrise } from "../../../shared/const/lists"
import { enumTypesInput } from "../../../shared/enums/enumTypesInput"
import { startUpdateVacancy } from "../../../store/vacancies/Vacancies.Thunk"
import { IFormlabels } from "zzz-folder03-componentes-react-ts/forms/IForm"
import { useBasesDispatch, useBasesSelector } from "../../../shared/store/store"
import { startSetColorAlert, startSetTextAlert, toogleModalFullScreenAction } from "../../../shared/hooks/useView"
import { enumAlerts } from "../../../shared/enums/enumAlerts"
import { enumAlertsColors } from "../../../shared/enums/enumColorAlerts"
import './UpdateVacancy.scss'
import { enumVacanciesAttributes } from "../../../shared/enums/enumVacanciesAttribute"

const labels: IFormlabels[] = [ 
	{ key: 'id', title: enumVacanciesAttributes.id },
    { key: 'idCitizen', title: enumVacanciesAttributes.idCitizen, placeholder: 'seleccione un ciudadano', typeInput: enumTypesInput.select, options: ListEnterphrise },
]

export const UpdateVacancy: FC = () => {

	const dispatch = useBasesDispatch()
	const { VacancyById } = useBasesSelector( state => state.Vacancies)
	const { onHandleBlur,
		    onCheckboxChange,
			onInputChange, 
			formState, 
			setFormState,
			onSelectChange,
			onMultipleSelectChange } = useForm ( VacancyById ) 
	
	useEffect( () => {
		setFormState(VacancyById)
	}, [ VacancyById ])
 
	const handleSubmit = async( event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault()
		dispatch( startUpdateVacancy( formState, VacancyById.id ))
		dispatch( startSetTextAlert( enumAlerts.successUpdate ))
        dispatch( startSetColorAlert( enumAlertsColors.success ))
		dispatch( toogleModalFullScreenAction() )
	}
	
	const handleCancel = () => {
        dispatch( toogleModalFullScreenAction())
    }
	
	const skyKeys = ["id"]

	return (
		<div className="update-vacancy">
			<Form
				botonCloseTitle={ "Cancelar" }
				botonSaveTitle={ "Actualizar vacante" } 
				labels={ labels }
				object={ formState }
				onClickCalcel={ handleCancel }			
				onInputChange={ onInputChange }
				onMultipleSelectChange={ onMultipleSelectChange }
				onSelectChange={ onSelectChange }
				onSubmit={ handleSubmit }
				skipkeys={ skyKeys }
				title={ "Actualizar vacante" }
				onCheckboxChange={ onCheckboxChange }
				onBlur={ onHandleBlur }
			/>
		</div>
	)
}