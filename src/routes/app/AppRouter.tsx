import { useEffect } from "react"
import { BrowserRouter as Router, Route, Routes } from "react-router-dom"
import { Alert } from "zzz-folder03-componentes-react-ts/alerts"
import { PagesRoutes } from "../pages/PagesRoutes"
import { startSetTextAlert } from "../../shared/hooks/useView"
import { useBasesDispatch, useBasesSelector } from "../../shared/store/store"
import './AppRouter.scss'

export const AppRouter = () => {
	const dispatch = useBasesDispatch()
	const { textAlert, isModalFullScreenOpen, colorAlert } = useBasesSelector( state => state.View )
		
	useEffect( () =>{
		const primeraAlerta = setTimeout(() => {
			dispatch( startSetTextAlert(''))
		}, 4000)
		
		return () => {
			clearTimeout(primeraAlerta)
		}
		
	}, [textAlert])
	
	useEffect(() => {
	    if( !isModalFullScreenOpen ){
			document.body.classList.remove('no-scroll-body')
	    }
		
	}, [ isModalFullScreenOpen ])
	

	return (
		<Router>
		
			{ textAlert.length >= 2 &&  <Alert textMessaje={ textAlert } colorAlert={ colorAlert } /> }
		
			<Routes>    
			    <Route path="/*" element={ <PagesRoutes /> } />
			</Routes>  
		 
		</Router>
	)
}
