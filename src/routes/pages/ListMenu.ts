import { IMenu } from "zzz-folder03-componentes-react-ts/navbars/Navbar"
import { enumPath } from "../../shared/enums/enumPath"

export const listMenus: IMenu[] = [
    {
        title: 'Ciudadanos',
        url: enumPath.citizens,
        iconClass: 'icon-users'
    },
    {
        title: 'Vacantes',
        url: enumPath.vacancies,
        iconClass: 'icon-projects'
    }
]