import { Route, Routes, Navigate } from "react-router-dom"
import { useBasesDispatch, useBasesSelector } from '../../shared/store/store'
import { useEffect } from "react"
import { enumPath } from "../../shared/enums/enumPath"
import { listMenus } from "./ListMenu"

import { Navbar } from "zzz-folder03-componentes-react-ts/navbars"
import './PagesRoutes.scss'
import { Header } from "../../sections/header/Header"
import { NotFound404 } from "../../modules/not-found-404/NotFound404"
import { Footer } from "../../sections/footer/Footer"
import { ContainerPagesLayout } from "./layout/ContainerPagesLayout"
import { VacanciePage } from "../../modules/vacancies/VacanciesPage"
import { CitizenPage } from "../../modules/citizenry/CitizenPage"

export const PagesRoutes = () => {

	const { isNavSmall } = useBasesSelector( state => state.View )

	return (
		<>
			<Header />
			{/*<Aside alerts={ alerts } isContainerAlertsOpen={ true }/>*/}

			<div className="pages-routes"> 
				<Navbar menus={ listMenus } isOnlyIcons={ isNavSmall } />
				<ContainerPagesLayout >
					<Routes>
						<Route path="/*" element={ <NotFound404 /> } />
						<Route path={ enumPath.vacancies } element={ <VacanciePage /> } />
						<Route path={ enumPath.citizens } element={ <CitizenPage/> } />
						
						<Route path="/" element={ <Navigate to={ enumPath.vacancies } /> } />
					</Routes> 
					<Footer />
				</ContainerPagesLayout>   
			</div>
		</>
	)
}
