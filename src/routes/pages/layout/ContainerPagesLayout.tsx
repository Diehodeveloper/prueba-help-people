import { FC } from 'react'
import './ContainerPagesLayout.scss'

interface IContainerPages {
    children: any,
}

export const ContainerPagesLayout: FC<IContainerPages> = ( { children } ) => {
    return (
        <div className='container-pages-layout'>
            { children }
        </div>
    )
}