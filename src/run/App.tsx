import { VerticalLine } from 'zzz-folder03-componentes-react-ts/lines'
import { AppRouter } from '../routes/app/AppRouter'

export const App = () => {
    return (
     <>
        <AppRouter />
        <VerticalLine />
        {/*<S02Footer />*/}
     </>
    )
}

