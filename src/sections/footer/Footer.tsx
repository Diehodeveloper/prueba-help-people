import { enumIcons } from '../../shared/enums/enumIcons'
import './Footer.scss'

export const Footer =  () => {

	return (
		<>	
			<footer className='footer'>
				<div className='footer__container-text'>
					<div className='footer__content-left'>
						<h3>Prueba frontend</h3>
						<h3>Created By Diego Rojas</h3>
					</div>
					<div className='footer__content-right'>
					<h3>React</h3><i className={ enumIcons.heart }></i>
					</div>	
				</div>
			</footer>  
		</>
	)
}
