import { enumIcons } from '../../shared/enums/enumIcons'
import { enumImages } from '../../shared/enums/enumImages'
import { toogleNavBarAction} from '../../shared/hooks/useView'
import { useBasesDispatch } from '../../shared/store/store'
import './Header.scss'

export const Header = () => {
	const dispatch = useBasesDispatch()

	const onHandleNavbarClick = () => {
		dispatch( toogleNavBarAction() )
	}
	return (
		<header className='header'>
			<i className={ enumIcons.list } onClick={ onHandleNavbarClick } />
			<img className='header__logo' src={ enumImages.logo } alt='logo' />
		</header>
	)
}	