import axios from "axios"
import { convertToCamelCase, convertToUnderscore } from "../functions/functions"

const axiosparam01baseUrl: string = 'http://localhost:3001'

export const getData = async (apiName: string): Promise<any> => {
	try {
		const response = await axios.get(`${axiosparam01baseUrl}${apiName}`)
		const convertedData = convertToCamelCase(response?.data)
		return convertedData
	} catch (error) {
		console.error(error)
	}
}

export const getDataById = async (apiName: string, id: number): Promise<any> => {
	try {
		const response = await axios.get(`${axiosparam01baseUrl}${apiName}${id}`)
		const convertedData = convertToCamelCase(response?.data)
		return convertedData
	} catch (error) {
		return error
	}
}

export const postData = async (apiName: string, object: any): Promise<any> => {
	try {
		const response = await axios.post(`${axiosparam01baseUrl}${apiName}`, convertToUnderscore( object ))
		return response
	} catch (error) {
		console.error(error)
	}
}

export const putData = async (apiName: string, id: number, object: any): Promise<any> => {
	try {
		const response = await axios.put(`${axiosparam01baseUrl}${apiName}${id}`, convertToUnderscore( object ))
		return convertToCamelCase(response?.data)
	} catch (error) {
		console.error(error)
	}
}

export const deleteData = async (apiName: string, id: number): Promise<any> => {
	try {
		const response = await axios.delete(`${axiosparam01baseUrl}${apiName}${id}`)
		return response
	} catch (error) {
		console.error(error)
	}
}


