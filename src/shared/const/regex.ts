export interface IRegex {
	title: string
	pattern: RegExp
}

export const notWhiteSpaces: IRegex ={
	title: 'No ingreses espacios en blanco al inicio !', 
	pattern: /^(?!\s).*/
}

