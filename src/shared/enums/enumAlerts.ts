export enum enumAlerts {
  error = 'hubo un error al realizar la solicitud !',
  welcome = 'Bienvenido Cabron !',
  invalidCredentials= 'La credenciales son incorrectas !',
  successDelete= 'Registro eliminado con exito !',
  successCreate= 'Registro creado con exito !',
  successUpdate= 'Registro actualizado con exito !',
  actionNotAvailable= 'Este registro no puede ser eliminado, consulte con el administrador',
}