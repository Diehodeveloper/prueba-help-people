

export enum enumCitizenAttribute {
  id= 'Id',
  documentType= 'Tipo de Documento',
  documentNumber= 'Número de documento',
  names= 'Nombres',
  surnames= 'Apellidos',
  birthday= 'Fecha de Nacimiento',
  proffesion= 'Profesión',
  salaryAspiration= 'Aspiración Salarial',
  email= 'Correo Electrónico'
} 
