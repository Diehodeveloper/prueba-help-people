
export enum enumAlertsColors {
  success= 'succes-color',
  warning= 'warning-color',
  error= 'error-color',
  info= 'info-color',
  gray= 'gray-color',
}