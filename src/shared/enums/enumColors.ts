
export enum enumColors {
  lightRed= 'var(--light-red)',
  yellow= 'var(--yellow)',
  green= 'var(--green)',
  lightGreen= 'var(--light-green)',
  red= 'var(--red)',
  blue= 'var(--light-blue)',
}