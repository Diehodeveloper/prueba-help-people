
export enum enumIcons {
	delete= `icon-delete`,
	update= 'icon-edit',
	avatar= 'src/assets/pngs/empty-state.png',
	list= 'icon-list',
	notifications= 'icon-notifications',
	dotsHorizontal= 'icon-dots-horizontal-triple',
	siwtch= 'icon-switch',
	heart= 'icon-heart',
	close= 'icon-close',
	bug= 'icon-bug',
	info= 'icon-info',
	warning= 'icon-warning',
	search= 'icon-search',
	circle= 'icon-circle',
	rightArrow= 'icon-right-arrow',
}