

export enum enumVacanciesAttributes {
  id= 'Id',
  code= 'Código',
  rol= 'Cargo',
  description= 'Descripción',
  enterprise= 'Empresa',
  salary= 'Salario',
  idCitizen= 'Ciudadano asignado'
} 
