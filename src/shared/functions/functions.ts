import { enumAlertsColors } from "../enums/enumColorAlerts"
import { PrimerFormato, SegundoFormato } from "../types/types"

export interface ConvertOptions {
	exclude?: string[] // propiedades a excluir del objeto de entrada
	include?: string[] // propiedades a incluir en el objeto de salida
}

export const convertToCamelCase = <T> (
	obj: PrimerFormato<T> | PrimerFormato<T>[],
	options: ConvertOptions ={}
    ): SegundoFormato<T> | SegundoFormato<T>[] => {
	const { exclude = [], include = [] } = options
	
	const convertItem = (item: PrimerFormato<T>): SegundoFormato<T> => {
		const convertedItem: SegundoFormato<T> ={}
		for (const prop in item) {
			if ( exclude.includes(prop) ) continue
			if ( include.length > 0 && !include.includes(prop) ) continue
			const convertedProp = prop.replace(/_(\w)/g, (_, c) => c.toUpperCase())
			convertedItem[convertedProp] = item[prop]
		}
		return convertedItem
	}

	if (Array.isArray(obj)) {
		return obj.map(convertItem)
	} else if (typeof obj === "object" && obj !== null) {
		return convertItem(obj)
	} else {
		console.error("Expected an array or object in convertToCamelCase function")
		return []
	}
}

export function convertToUnderscore <T>(obj: T): T {
	const result = {} as T;
	for (let key in obj) {
	  const underscoreKey = key.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();
	  result[ underscoreKey as keyof T ] = obj[ key as keyof T ];
	}
	return result;
}


export const getRandomColor = () => {
	const colors = [ '#F9483F', '#FDC104', '#4C47B8', '#0976FE' ]
	const randomIndex = Math.floor( Math.random() * colors.length )
	return colors[ randomIndex ]
}

export const getRandomColorAlert = () => {
	const colors = [ enumAlertsColors.success, enumAlertsColors.info, enumAlertsColors.gray ]
	const randomIndex = Math.floor( Math.random() * colors.length )
	return colors[ randomIndex ]
}

export const parseDate = (dateStr: string): Date => {
	const [ day, month, year, hour, minute, second ] = dateStr.split(/[/: ]/).map(Number)
	return new Date(year, month - 1, day, hour, minute, second)
}

export const getTimeDiff = (time: string): string => {
	const now = new Date()
	const logTime = parseDate(time)
	const diff = Math.abs(now.getTime() - logTime.getTime())
	const seconds = Math.floor(diff / 1000)
	
	if (seconds < 60) {
		return `${seconds} segundo${seconds !== 1 ? 's' : ''}`
	}

	const minutes = Math.floor(seconds / 60)
	
	if (minutes < 60) {
		return `${minutes} minuto${minutes !== 1 ? 's' : ''}`
	}

	const hours = Math.floor(minutes / 60)
	
	if (hours < 24) {
		return `${hours} hora${hours !== 1 ? 's' : ''}`
	}

	const days = Math.floor(hours / 24)
	
	return `${days} día${days !== 1 ? 's' : ''}`  
}

export const getCurrentYear = (): number => {
	return new Date().getFullYear();
}

export const listToObjectArray = ( object: any, key: string ) => {	

	interface Option {
		key: string
		value: string
		label: string
	}

	if (!! object ) {

		const keys =  object.split("|")
		const result: Option[] = keys.map( ( item: string ) => { 
			return {
				key: key,
				value: item,
				label: item
			}
		})
		
	return result
	}
}

export const valueToObjectArray = (object: any, key: string, options: any) => {
	interface Option {
	  key: string;
	  value: string;
	  label: string;
	}
  
	if (!!object) {
	  const currentItem = options.find(( option: Option) => option.value == object );
  
	  if (!!currentItem) {
		const label = typeof object === "number" ? currentItem.label : object;
  
		const result = {
		  key: key,
		  value: object,
		  label: label,
		};
  
		return result;
	  }
	}
  }
  
export const getValueFromEnumString = <T extends { [key: string]: string | number }>(enumString: string, enumContent: T) => {
	const enumEntry = Object.entries(enumContent).find(([key, value]) =>
	key === enumString); if (!!!enumEntry) return null;
	return { name: enumEntry[0], value: enumEntry[1] }
}
