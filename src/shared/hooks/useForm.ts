import { useState } from 'react'

export function useForm<T>( initState: T) {
	const [formState, setFormState] = useState( initState )
	const [selectList, setSelectList] = useState<string[]>([])
	
	const onInputChange: React.ChangeEventHandler<HTMLInputElement> = ({ target }) => {
		const { name, value } = target
		setFormState({
			...formState,
			[name]: value,
		})
	}
	
	const onHandleBlur: React.FocusEventHandler<HTMLInputElement> = ({ target }) => {
		const { name, value } = target
		setFormState({
			...formState,
			[name]: value.trim(),
		})
	}

	const onMultipleSelectChange = (options: any) => {
	
		const values = options.map((item: any) => item.label).join('|')
		const name = options.length > 0 ? options[0].key : null
		
		setSelectList(values)
		
		if (name !== null) {
		  setFormState({
			...formState,
			[name]: values,
		  })
		}
	}
	
	const onSelectChange = (options: any) => {
		setFormState({
			...formState,
			[ options.key]: options.value
		})
	}
	
	const onCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, checked } = e.target
		setFormState({
		  ...formState,
		  [name]: checked,
		})
    }

	const onResetForm = () => {
		setFormState(initState)
	}

	return {
		...formState,
		formState,
		onInputChange,
		onResetForm,
		setFormState,
		onMultipleSelectChange,
		selectList,
		onSelectChange,
		onCheckboxChange,
		onHandleBlur
	}
}
