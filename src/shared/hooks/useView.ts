import { enumAlerts } from "../enums/enumAlerts"
import { enumAlertsColors } from "../enums/enumColorAlerts"
import { Dispatch, AnyAction } from "redux"
import { toogleNavBar, setIdForPost, setTextAlert, toogleModalFullScreen, setPostOption, setColorAlert } from "../slices/view.Slice"
		
	export const toogleNavBarAction = () => {
		return( dispatch: Dispatch<AnyAction> ) => {
			dispatch(toogleNavBar())
		}
	}

	export const toogleModalFullScreenAction = () => {
		return( dispatch: Dispatch<AnyAction> ) => {
			dispatch( toogleModalFullScreen())
		}
	}	
		
	export const setIdForPostAction = ( id: number | undefined ) => {
		return( dispatch: Dispatch<AnyAction> ) => {
			dispatch( setIdForPost({ id: id }))
		}
	}
	
	export const startSetTextAlert = ( text: string ) => {
		return( dispatch: Dispatch<AnyAction> ) => {
			dispatch( setTextAlert( text ))
		}
	}
	
	export const startSetColorAlert = ( text: string ) => {
		return( dispatch: Dispatch<AnyAction> ) => {
			dispatch( setColorAlert( text ))
		}
	}
	
	export const setPostOptionAction = ( id: number ) => {
		return( dispatch: Dispatch<AnyAction> ) => {
			dispatch( setPostOption( id ))
		}
	}
	
	export const sendError =  () => {
		return( dispatch: Dispatch<AnyAction> ) => {
			startSetTextAlert( enumAlerts.error)
			startSetColorAlert( enumAlertsColors.error ) 
		}
	}
	
	export const sendSuccess =  ( typeMessaje: enumAlerts ) => {
		return( dispatch: Dispatch<AnyAction> ) => {
			startSetTextAlert( typeMessaje )
			startSetColorAlert( enumAlertsColors.success ) 
		}
	}
	