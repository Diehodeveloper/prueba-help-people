import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { enumAlertsColors } from '../enums/enumColorAlerts'

interface InterfaceView {
	colorAlert: string
	idForPost: number
	isModalConfirmationOpen: boolean
	isModalFullScreenOpen: boolean
	isNavSmall: boolean
	pageActive: number
	postOption: number
	textAlert: string
}

const initialState: InterfaceView ={
	isModalFullScreenOpen: false,
	isModalConfirmationOpen: false,
	isNavSmall: true,
	pageActive: 1,
	textAlert: '',
	idForPost: 0,
	postOption: 0,
	colorAlert: enumAlertsColors.success
}

export const sliceView = createSlice({
  name: 'view',
  initialState,
  reducers: {
    toogleNavBar: (state) => {
      	state.isNavSmall = !state.isNavSmall
    },
    toogleModalFullScreen: (state) => {
      	state.isModalFullScreenOpen = !state.isModalFullScreenOpen
    },
    setIdForPost: (state, action) => {
      	state.idForPost = action.payload.id
    },
    setPage: (state, action: PayloadAction<number>) => {
      	state.pageActive = action.payload
    },
    setPostOption: (state, action: PayloadAction<number>) => {
      	state.postOption =  action.payload
    },
    setTextAlert: (state, action: PayloadAction<string>) => {
      	state.textAlert = action.payload
    },
    setColorAlert: (state, action: PayloadAction<string>) => {
      	state.colorAlert = action.payload
    },
  },
})

export const {
	setIdForPost,
	setPage,
	setPostOption,
	setTextAlert,
	toogleModalFullScreen,
	toogleNavBar,
	setColorAlert,
 } = sliceView.actions
