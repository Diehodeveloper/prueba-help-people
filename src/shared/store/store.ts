import { configureStore } from '@reduxjs/toolkit'
import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux'
import { sliceView } from '../slices/view.Slice'
import { sliceCitizens } from '../../store/citizens/Citizens.Slice'
import { sliceVancies} from '../../store/vacancies/Vacancies.Slice'

export const store= configureStore({
	reducer: {
		Citizens: sliceCitizens.reducer,
		Vacancies: sliceVancies.reducer,
		View: sliceView.reducer,
	},
})  

type DispatchFunc = () => BasesDispatch
export type BasesDispatch = typeof store.dispatch
export const useBasesDispatch: DispatchFunc = useDispatch
export type BasesStateType = ReturnType<typeof store.getState>
export const useBasesSelector: TypedUseSelectorHook<BasesStateType> = useSelector



