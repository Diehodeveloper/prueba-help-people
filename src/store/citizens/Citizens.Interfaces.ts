export interface Citizen {
	id: number
	documentType: string
	documentNumber: number
	names: string
	surnames: string
	birthday: string
	profession: string
	salarialAspiration: number
	email: string
}

export interface Citizens {
	Citizens: Citizen[],
	CiticieById: Citizen
}