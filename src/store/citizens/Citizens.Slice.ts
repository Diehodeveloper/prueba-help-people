import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { enumAlerts } from '../../shared/enums/enumAlerts'
import { Citizens, Citizen } from './Citizens.Interfaces'

const initialState: Citizens = {
	Citizens: [],
	CiticieById: {
		id: 0,
		documentType: '',
		documentNumber: 0,
		names: '',
		surnames: '',
		birthday: '',
		profession: '',
		salarialAspiration: 0,
		email: '',
	}
}

export const sliceCitizens = createSlice({
	name: 'citizens',
	initialState,
	reducers: {
		getCitizens: (state, action: PayloadAction<{ Citizens: Citizen[] }>) => {
			state.Citizens = action.payload.Citizens
		},
		getCitizenById: (state, action: PayloadAction<{ citizen: Citizen }>) => {
			if ( !! action.payload.citizen ) {
				state.CiticieById = action.payload.citizen
			}   
		},
		resetCitizenById: ( state  ) => {
			state.CiticieById = initialState.CiticieById
		},
		createCitizen: (state, action: PayloadAction<{ citizen: Citizen }>) => {
			state.Citizens = [ ...state.Citizens, action.payload.citizen ]
		},
		updateCitizen: (state, action: PayloadAction<{ citizen: Citizen }>) => {
				state.Citizens = state.Citizens.map( (citizen) =>
				citizen.id === action.payload.citizen.id ? action.payload.citizen : citizen
			)
		},
		deleteCitizen: (state, action: PayloadAction< { message: enumAlerts, id: number }>) => {
			if ( action.payload.message ===  enumAlerts.successDelete ) {
				state.Citizens = state.Citizens.filter( ( citizen ) => citizen.id != action.payload.id )
				state.CiticieById = initialState.CiticieById
			}	
		},
	},
})

export const {
	createCitizen,
	deleteCitizen,
	getCitizens,
	getCitizenById,
	updateCitizen,
	resetCitizenById
} = sliceCitizens.actions
