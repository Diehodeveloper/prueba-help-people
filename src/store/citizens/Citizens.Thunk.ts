import { deleteData, getData, getDataById, postData, putData } from '../../shared/axios/axios'
import { enumApisNames } from "../../shared/enums/enumApisNames"
import { enumAlertsColors } from "../../shared/enums/enumColorAlerts"
import { enumAlerts } from "../../shared/enums/enumAlerts"
import { Dispatch, AnyAction } from "redux"
import { sendError } from "../../shared/hooks/useView"
import { setColorAlert, setTextAlert } from "../../shared/slices/view.Slice"
import { Citizen } from "./Citizens.Interfaces"
import { createCitizen, deleteCitizen, getCitizenById, getCitizens, resetCitizenById, updateCitizen } from './Citizens.Slice'

export const startGetCitizens = () => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			const response = await getData( enumApisNames.users )
			dispatch( getCitizens({ Citizens: response }))      
		} catch (error) {
			sendError()
		}
	}
}

export const startGetCitizenById = ( id: number ) => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			const citizen = await getDataById( enumApisNames.users, id)	
			dispatch( getCitizenById({ citizen: citizen }))   
		   
		} catch (error) {
			sendError()
		}
	}
}

export const startResetCitizenById = () => {
	return ( dispatch: Dispatch<AnyAction> ) => {
		try {
			dispatch( resetCitizenById() )
		} catch (error) {
			sendError()
		}
	}	
}

export const startCreateCitizen = ( citizen: Citizen ) => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			const response = await postData( enumApisNames.users, citizen )
			dispatch( createCitizen({ citizen: response.data })) 
			dispatch( setColorAlert( enumAlertsColors.success ) )
			dispatch( setTextAlert( enumAlerts.successCreate ) )
		} catch (error) {
			sendError()
		}
	}	
}

export const startUpdateCitizen = ( citizen: Citizen, id: number ) => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			debugger
			const response = await putData( enumApisNames.users, id, citizen)
			dispatch( updateCitizen({ citizen: response }))  
			dispatch( setColorAlert( enumAlertsColors.success ) )
			dispatch( setTextAlert( enumAlerts.successUpdate ) )
		} catch (error) {
			sendError()
		}
	}	
}

export const startDeleteCitizen = ( id: number )  => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			await deleteData( enumApisNames.users , id)
			dispatch( deleteCitizen( { message: enumAlerts.successDelete, id: id })) 
			dispatch( setColorAlert( enumAlertsColors.success ) )
			dispatch( setTextAlert( enumAlerts.successDelete ) )	
			 
		} catch (error) {
			dispatch( deleteCitizen({ message: enumAlerts.error, id: id }))  
			dispatch( setColorAlert( enumAlertsColors.error ) )
			dispatch( setTextAlert( enumAlerts.error) )
		}
	}
}
