export interface Vacancy {
	id: number
	code: string
	rol: string
	description: string
	enterprise: string
	salary: number
	IdCitizen: string
}

export interface Vacancies {
	Vacancies: Vacancy[]
	VacancyById: Vacancy
}