import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Vacancies, Vacancy } from './Vacancies.Interfaces'

const initialState: Vacancies = {
	Vacancies: [],
	VacancyById: {
		id: 0,
		code: '',
		rol: '',
		description: '',
		enterprise: '',
		salary: 2,
		IdCitizen: 'test'
	}
}

export const sliceVancies = createSlice({
	name: 'vacancies',
	initialState,
	reducers: {
		onGetVacancies: (state, action: PayloadAction<{ projects: Vacancy[] }>) => {
			state.Vacancies = action.payload.projects
		},
		onGetVacancyById: (state, action: PayloadAction<{ projects: Vacancy }>) => {
			state.VacancyById = action.payload.projects
		},
		onResetVacancyById: (state) => {
			state.VacancyById = initialState.VacancyById
		},
		onUpdateVacancy: (state, action: PayloadAction<{ vacancy: Vacancy }>) => {
			state.Vacancies = state.Vacancies.map((vacancy) =>
				vacancy.id === action.payload.vacancy.id ? action.payload.vacancy: vacancy
			)
		},
	},
})

export const {
	onGetVacancies,
	onGetVacancyById,
	onUpdateVacancy,
	onResetVacancyById
} = sliceVancies.actions
