import { deleteData, getData, getDataById, postData, putData } from '../../shared/axios/axios'
import { enumApisNames } from "../../shared/enums/enumApisNames"
import { enumAlerts } from "../../shared/enums/enumAlerts"
import { AnyAction, Dispatch } from 'redux'
import { sendError, sendSuccess } from '../../shared/hooks/useView'
import { onGetVacancies, onGetVacancyById, onResetVacancyById, onUpdateVacancy } from './Vacancies.Slice'
	
export const startGetVacancies = () => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			const response = await getData( enumApisNames.vacancies )
			dispatch( onGetVacancies({ projects: response }))
		} catch (error) {
			sendError()
		}
	}	
}

export const startGetVacancyId = ( id: number ) => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			const response = await getDataById( enumApisNames.vacancies, id)
			dispatch( onGetVacancyById({ projects: response }))      
		} catch (error) {
			sendError()
		}
	}
}

export const startResetProjectById = () => {
	return ( dispatch: Dispatch<AnyAction> ) => {
		try {
			dispatch( onResetVacancyById() )
		} catch (error) {
			sendError()
		}
	}	
}

export const startUpdateVacancy= ( vacancy: object , id: number ) => {
	return async( dispatch: Dispatch<AnyAction> ) => {
		try {
			debugger
			const response = await putData( enumApisNames.vacancies, id, vacancy)
			sendSuccess( enumAlerts.successUpdate )
			dispatch( onUpdateVacancy({ vacancy: response }))    
		
		} catch (error) {
			sendError()
		}
	}
}